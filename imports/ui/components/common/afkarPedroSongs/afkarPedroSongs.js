import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarPedroSongs.html';
import './afkarPedroSongs.less';

class AfkarPedroSongs {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);
    
    var audio=new Audio();

    this.play=function(){
      audio.src=this.songUrl;
      audio.play();         
    }

    this.muteControl=function(){
    
      if(audio.volume==0){
        audio.volume=1;
      } else  {
        audio.volume=0;
      }               
    }
    this.helpers({
      afkar(){
      }

    })
  } 
  
}

const name = 'afkarPedroSongs';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  bindings: {
    songUrl: '@' 
  },
  controller: ['$reactive', '$scope', AfkarPedroSongs],
  controllerAs: "song"
});
