import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarContact.html';
import './afkarContact.less';



class AfkarContact {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);
    this.backgroundStyle = {
      "background": "url(" + this.imgUrl + ")",
      "background-size" : "cover",
      "background-position" : "center"
    };

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarContact';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  bindings: {
      imgUrl: '@'
    },
  controller: ['$reactive', '$scope', AfkarContact],
  controllerAs: "ctrl"
});
