import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './banner.html';
import './banner.less';



class Banner {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);
    console.log(this.imgUrl);
    this.backgroundStyle = {
      "background": "url(" + this.imgUrl + ")",
      "background-size" : "cover",
      "background-position" : "center"
    };
    console.log(this.backgroundStyle)
    // $("banner>div").css("background", "url(" + this.imgUrl + ")");
    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'banner';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  bindings: {
    imgUrl: '@'
  },
  controller: ['$reactive', '$scope', Banner],
  controllerAs: "ctrl"
});
