import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarSongs.html';
import './afkarSongs.less';



class AfkarSongs {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarSongs';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarSongs],
  controllerAs: "ctrl"
});
