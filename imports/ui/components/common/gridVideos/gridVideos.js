import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './gridVideos.html';
import './gridVideos.less';



class GridVideos {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'gridVideos';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', GridVideos],
  controllerAs: "ctrl"
});
