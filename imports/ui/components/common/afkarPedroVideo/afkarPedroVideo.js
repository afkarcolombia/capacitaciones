import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarPedroVideo.html';
import './afkarPedroVideo.less';




class AfkarPedroVideo {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  } 
  
}

const name = 'afkarPedroVideo';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  bindings: {
    songUrl: '@' 
  },
  controller: ['$reactive', '$scope', AfkarPedroVideo],
  controllerAs: "video"
});
