import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarAlbertSongs.html';
import './afkarAlbertSongs.less';



class AfkarAlbertSongs {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);
    console.log(this.songUrl);
   var audio=new Audio();

    this.reproducir=function(){
     audio.src=this.songUrl;
      audio.play();         
    }

    this.controlSonido=function(){
    
     if(audio.volume==0)
     {audio.volume=1;}
     else{
       audio.volume=0;
      }               
    }
    this.helpers({
      afkar(){
      }

    })
  } 
  
}

const name = 'afkarAlbertSongs';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  bindings: {
    songUrl: '@' 
  },
  controller: ['$reactive', '$scope', AfkarAlbertSongs],
  controllerAs: "song"
});
