import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarTitleVideos.html';
import './afkarTitleVideos.less';



class AfkarTitleVideos {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarTitleVideos';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarTitleVideos],
  controllerAs: "ctrl"
});
