import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarAbout.html';
import './afkarAbout.less';



class AfkarAbout {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarAbout';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarAbout],
  controllerAs: "ctrl"
});
