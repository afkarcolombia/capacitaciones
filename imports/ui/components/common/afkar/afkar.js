import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngMaterial from 'angular-material';
import uiRouter from 'angular-ui-router';

import template from './afkar.html';
import {name as AfkarNav} from '/imports/ui/components/common/afkarNav/afkarNav';
import {name as AfkarAbout} from '/imports/ui/components/common/afkarAbout/afkarAbout';
import {name as AfkarBanner} from '/imports/ui/components/common/afkarBanner/afkarBanner';
import {name as AfkarFooter} from '/imports/ui/components/common/afkarFooter/afkarFooter';
import { name as Home } from '/imports/ui/views/home/home';
import { name as About } from '/imports/ui/views/about/about';
import { name as Contact } from '/imports/ui/views/contact/contact';
import { name as Nav } from '/imports/ui/components/common/nav/nav';

class Afkar {}

const name = 'afkar';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  ngMaterial,
  AfkarNav,
  AfkarAbout,
  AfkarBanner,
  AfkarFooter,
  About,
  Nav,
  Contact,
  Home
]).component(name, {
  template: template,
  controllerAs: name,
  controller: Afkar
})
  .config(config);

function config($locationProvider, $urlRouterProvider, $mdThemingProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/afkar');

  $mdThemingProvider.theme('default')
  .primaryPalette('grey')
  .accentPalette('pink');
}