import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarTitleSongs.html';
import './afkarTitleSongs.less';



class AfkarTitleSongs {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarTitleSongs';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarTitleSongs],
  controllerAs: "ctrl"
});
