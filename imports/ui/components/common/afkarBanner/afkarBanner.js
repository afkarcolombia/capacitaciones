import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarBanner.html';
import './afkarBanner.less';



class AfkarBanner {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarBanner';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarBanner],
  controllerAs: "ctrl"
});
