import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarFooter.html';
import './afkarFooter.less';



class AfkarFooter {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarFooter';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarFooter],
  controllerAs: "ctrl"
});
