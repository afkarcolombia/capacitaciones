import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';



import template from './home.html';
import './home.less';
import { name as Banner } from "../../components/common/banner/banner"
import { name as GridVideos } from "../../components/common/gridVideos/gridVideos"
import { name as AfkarTitleVideos} from "../../components/common/afkarTitleVideos/afkarTitleVideos"
import { name as AfkarSongs } from "../../components/common/afkarSongs/afkarSongs"
import { name as AfkarTitleSongs } from "../../components/common/afkarTitleSongs/afkarTitleSongs"
import { name as AfkarFooter } from "../../components/common/afkarFooter/afkarFooter"

import { name as AfkarAlbertSongs } from "../../components/common/afkarAlbertSongs/afkarAlbertSongs"
import { name as AfkarPedroSongs } from "../../components/common/afkarPedroSongs/afkarPedroSongs";
import { name as AfkarPedroVideo } from "../../components/common/afkarPedroVideo/afkarPedroVideo";

class Home {
	constructor() {
	}
}

const name = 'home';

export default angular.module(name, [
	angularMeteor,
	uiRouter,
	Banner,
	GridVideos,
	AfkarTitleVideos,
	AfkarSongs,
	AfkarTitleSongs,
	AfkarFooter,
	AfkarAlbertSongs,
	AfkarPedroSongs,
	AfkarPedroVideo
	])
.component(name, {
	template: template,
	controller: Home,
	controllerAs: "ctrl"
})
.config(config);

function config($stateProvider) {
	'ngInject';
	$stateProvider
	.state(name, {
		url: '/afkar',
		template: '<home></home>'
	});
}
