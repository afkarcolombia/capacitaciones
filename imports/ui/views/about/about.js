import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './about.html';
import './about.less';

import {name as AfkarAbout} from '/imports/ui/components/common/afkarAbout/afkarAbout';
import {name as Banner} from '/imports/ui/components/common/banner/banner';
import {name as Nav} from '/imports/ui/components/common/nav/nav';
import {name as AfkarFooter} from '/imports/ui/components/common/afkarFooter/afkarFooter';


class About {
	constructor() {
	}
}

const name = 'about';

export default angular.module(name, [
	angularMeteor,
	uiRouter,
	Nav,
  AfkarAbout,
  Banner,
  AfkarFooter,
  
	])
.component(name, {
	template: template,
	controller: About,
	controllerAs: "ctrl"
})
.config(config);

function config($stateProvider) {
	'ngInject';
	$stateProvider
	.state(name, {
		url: '/about',
		template: '<about></about>'
	});
}
