import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './contact.html';
import './contact.less';

import {name as AfkarNav} from '/imports/ui/components/common/afkarNav/afkarNav';
import {name as Banner} from '/imports/ui/components/common/banner/banner';
import {name as Nav} from '/imports/ui/components/common/nav/nav';
import {name as AfkarFooter} from '/imports/ui/components/common/afkarFooter/afkarFooter';
import {name as AfkarContact} from '/imports/ui/components/common/afkarContact/afkarContact';

class Contact {
	constructor() {
	}
}

const name = 'contact';

export default angular.module(name, [
	angularMeteor,
	uiRouter,
	Nav,
  Banner,
  AfkarFooter,
  AfkarContact
	])
.component(name, {
	template: template,
	controller: Contact,
	controllerAs: "ctrl"
})
.config(config);

function config($stateProvider) {
	'ngInject';
	$stateProvider
	.state(name, {
		url: '/contact',
		template: '<contact></contact>'
	});
}
